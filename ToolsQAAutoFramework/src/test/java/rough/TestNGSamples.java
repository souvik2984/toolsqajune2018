package rough;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGSamples {
	
	private ChromeDriver driver;
	
	@BeforeClass
	public void getBrowser() {
		System.setProperty("webdriver.chrome.driver", "C:\\ToolsQAJARS\\chromedriver.exe");
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);

		// page load timeout
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

		// implicit wait
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}
	
	@BeforeMethod
	public void launchApp() {
		if(driver!=null) 
		driver.get("https://www.tripadvisor.ie/SmartDeals-g186605-Dublin_County_Dublin-Hotel-Deals.html");
	}
	
	
	@Test
	public void ReturnHotelInformationFromTripAdvisor() {
		waitForElement(driver,driver.findElement(By.xpath(".//h1[@class='header heading masthead masthead_h1 ']"))).click();
		
		ScrollToElement(driver);
		
		List<WebElement> hotels = driver.findElements(By.xpath(".//div[contains(@class,'listItem')]"));
			
		int counter = 0; WebElement ele;
		
		for(int i=0; i <= hotels.size()-1; i++) {
			
			List<WebElement> hotel = driver.findElements(By.xpath(".//div[contains(@class,'listItem')]"));
				
			System.out.println("Hotel Name : "+ waitForElement(driver,hotel.get(i).findElement(By.xpath("descendant::div[@data-prwidget-name='meta_hsx_listing_name']"))).getText());
			System.out.println("Current Price : "+ waitForElement(driver,hotel.get(i).findElement(By.xpath("descendant::div[contains(@class,'allowEllipsis')]/div[2]//div[@data-sizegroup='mini-meta-price']"))).getText());				
			System.out.println("Reviews : "+ waitForElement(driver, hotel.get(i).findElement(By.xpath("descendant::a[contains(text(),'reviews')]"))).getText());
			
			System.out.println(" ============================================================ ");
			counter = i;	
		}
		
		System.out.println("No of hotels present : "+ counter);
		
	}
	
	@AfterClass
	public void quitDriver() {
		if(driver!=null) driver.quit();
	}
	
	private WebElement waitForElement(WebDriver driver, WebElement ele) {	
		return new WebDriverWait(driver,10)
				    .ignoring(StaleElementReferenceException.class)
				    .until(ExpectedConditions.elementToBeClickable(ele));
	}
	
	private void ScrollToElement(WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1000)");
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
	}
	
	
	

}
