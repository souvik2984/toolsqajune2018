package rough;

import rough2.ISwissBank;

public interface ICentralBank extends ISwissBank{
	
	public void performDeposit() ;
	public void performWithdrawal() ;
	public void calcInterest();
	

}
