package rough;

public class TestAbstract {

	public static void main(String[] args) {
		
		HdfcBank hdfc = new HdfcBank();
		hdfc.performDeposit("HDFC");
		hdfc.performWithdrawal("HDFC");
		hdfc.calcInterest();
		
		AXISBank axis = new AXISBank();
		axis.performDeposit("AXIS");
		axis.performWithdrawal("AXIS");
		axis.calcInterest();
		
	}

}
