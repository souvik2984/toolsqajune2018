package rough;

public abstract class CentalBank {
	
	public CentalBank() {
		System.out.println("test me");
	}
	
	public void performDeposit(String bankName) {
		System.out.println("Performing deposit for the bank : "+ bankName);
	}
	
	public void performWithdrawal(String bankName) {
		System.out.println("Performing withdrawal for the bank : "+ bankName);
	}
	
	abstract public void calcInterest ();
	

}
