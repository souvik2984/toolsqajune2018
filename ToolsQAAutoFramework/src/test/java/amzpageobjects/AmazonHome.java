package amzpageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import util.RepStatus;
import util.Reporter;
import util.Web;

public class AmazonHome {
	
	@FindBy(xpath=".//select[@id='searchDropdownBox']")
	private WebElement selCategory;
	
	@FindBy(xpath=".//input[@id='twotabsearchtextbox']")
	private WebElement txtProductSearch;
	
	@FindBy(xpath=".//input[@value='Go']")
	private WebElement btnGO;
	
	private WebDriver driver;
	
	public AmazonHome(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void launchApp(String URL) {
		driver.get(URL);
	}
	
	public SearchResult searchProduct(String cat, String product) {
		Web.selectElementByVisibleText(selCategory, cat);
		Web.sendText(txtProductSearch, product);
		Web.clickElement(btnGO);
		Reporter.log(RepStatus.INFO,"Category "+cat+" with Product "+product+" successfully searched", 
				"Product search successfull");
		return new SearchResult(driver);
	}

	
	
	
	

}
