package amzpageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import util.RepStatus;
import util.Reporter;
import util.Web;

public class ProductDetails {

	@FindBy(xpath=".//span[@id='productTitle']")
	WebElement txtProductName;
	
	WebDriver driver;
	
	public ProductDetails(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public boolean verifyProductSearch(String prodName) {
		try {
			return Web.getText(txtProductName).equals(prodName);
		}catch(Exception e) {
			Reporter.log(RepStatus.FAIL, "Exception", e.getMessage());
		}
		return false;
	}
	
	
}
