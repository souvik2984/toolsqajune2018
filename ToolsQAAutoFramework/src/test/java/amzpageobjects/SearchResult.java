package amzpageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import util.RepStatus;
import util.Reporter;
import util.Web;

public class SearchResult {

	@FindBy(xpath=".//ul[@id='s-results-list-atf']/li")
	List<WebElement> searchResults;
	
	@FindBy(xpath=".//span[@id='pagnNextString']")
	WebElement pageNext;
	
	private WebDriver driver;
	
	public SearchResult(WebDriver driver) {
		this.driver =  driver;
		PageFactory.initElements(driver, this);
	}
	
	public ProductDetails clickFirstResult() {
		WebElement res = searchResults.get(0).findElement(By.xpath("//h2/.."));
		String result =  Web.getText(res);
		Reporter.log(RepStatus.INFO,"Product "+result+" is to be verified", 
				"Product from search result to be verified");
		Web.clickElement(res);
		Web.setTempVal(result);
		return new ProductDetails(driver);
	}

	public void navigateToPagination(int itr) {
		Web.waitForElement(driver,pageNext,10);
		for(int i=1;i<=itr;i++) {
			Web.clickElement(pageNext);
		}
	}
	
	
	
}
