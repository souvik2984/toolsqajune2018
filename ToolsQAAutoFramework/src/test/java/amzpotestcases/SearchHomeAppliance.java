package amzpotestcases;

import org.testng.Assert;
import org.testng.annotations.Test;

import amzpageobjects.AmazonHome;
import amzpageobjects.ProductDetails;
import amzpageobjects.SearchResult;
import core.Lib;
import core.TestBase;
import util.RepStatus;
import util.Reporter;
import util.Web;

public class SearchHomeAppliance extends TestBase{
	
	AmazonHome home;
	SearchResult result;
	ProductDetails prod;
	
	@Test
	public void VerifyProductNameForMixerGrinder() {
		try {
			for(int itr=0;itr<dataList.size()-1;itr++) {
				home = new AmazonHome(driver);
				home.launchApp(Lib.getProperty("URL"));

				result = home.searchProduct(dataList.get(itr).get("Category")
						                   ,dataList.get(itr).get("ProductType"));
				int i =  1;
				i= i/0;
				
				prod = result.clickFirstResult();
				if(prod.verifyProductSearch(Web.getTempVal())) {
					Reporter.log(RepStatus.PASS,"Verify Product Name as displayed in result page",
							 "Product name verified successfully");
				}else {
					Reporter.log(RepStatus.FAIL,"Verify Product Name as displayed in result page",
							 "Product name verification failed");
				}
			}
		}catch(Exception e) {
			reportException(e);
		}
		
	}
	
	@Test
	public void VerifyPaginationSearchForMixer() {	
		try {
			home = new AmazonHome(driver);
			home.launchApp(Lib.getProperty("URL"));
			result = home.searchProduct(dataList.get(0).get("Category"), 
								dataList.get(0).get("ProductType"));
			
			result =  new SearchResult(driver);
			result.navigateToPagination(Integer.parseInt(dataList.get(0).get("Pagination")));
			prod = result.clickFirstResult();
			
			prod = new ProductDetails(driver);
			if(prod.verifyProductSearch(Web.getTempVal())) {
				Reporter.log(RepStatus.PASS,"Verify Product Name as displayed in result page",
						 "Product name verified successfully");
			}else {
				Reporter.log(RepStatus.FAIL,"Verify Product Name as displayed in result page",
						 "Product name verification failed");
			}
		}catch(Exception e) {
			reportException(e);
		}
	}
	
	
}
