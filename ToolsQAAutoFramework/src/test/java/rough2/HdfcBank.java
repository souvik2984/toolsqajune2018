package rough2;


public class HdfcBank implements ICentralBank{

	
	public void performDeposit() {
		System.out.println("Perform deposit for HDFC Bank");
	}

	public void performWithdrawal() {
		System.out.println("Perform withdrawal for HDFC Bank");
	}
	
	public void calcInterest() {
		System.out.println("Calculate interest for HDFC Bank at the rate of 5.0 %");
	}
	
	public void testHDFC() {
		
	}


}
