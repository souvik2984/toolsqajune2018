package rough2;

import java.util.HashMap;
import java.util.Map;

import util.XL_ReadWrite;

public class TestDataExtract {

	public static void main(String[] args) {
		try {

			ExtractData("VerifyProductNameForMixerGrider");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Map<String,String> ExtractData(String testCaseName) {
		Map<String,String> data = new HashMap<>();
		try {
			XL_ReadWrite xl = new XL_ReadWrite(System.getProperty("user.dir") + "//TestData.xlsx");

			for (int iRow = 0; iRow < xl.getRowCount(0); iRow++) {
				if (xl.getCellData(0, iRow, 0).equals(testCaseName)) {
					for (int iCol = 1; iCol < xl.getColCount(iRow, 0); iCol++) {
						data.put(xl.getCellData(0, iRow, iCol), xl.getCellData(0, iRow+1, iCol));
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}
