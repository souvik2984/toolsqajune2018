package rough2;

public class AXISBank implements ICentralBank{


	public void performDeposit() {
		System.out.println("Perform deposit for AXIS Bank");	
	}

	public void performWithdrawal() {
		System.out.println("Perform withdrawal for AXIS Bank");
	}
	
	public void calcInterest() {
		System.out.println("Calculate interest for AXIS Bank at the rate of 6.0 %");
	}

	public void megaDeposit() {
		System.out.println("Perform mega deposit for AXIS Bank");	
	}

}
