package core;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class Lib {
	
	private static Properties configProp;
	
	public static String getProperty(String prop) {
		if(!configProp.isEmpty() && configProp.containsKey(prop.toUpperCase())) {
			return configProp.getProperty(prop.toUpperCase());
		}
		return "";
	}
	
	public static void loadProperty() {
		try {
			configProp = new Properties();
			FileInputStream fin = new FileInputStream(
					new File(System.getProperty("user.dir")+"\\TestProperties.properties"));
			configProp.load(fin);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
