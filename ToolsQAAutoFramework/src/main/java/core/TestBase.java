package core;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import util.RepStatus;
import util.Reporter;
import util.XL_ReadWrite;

public class TestBase {

	protected WebDriver driver;
	protected List<Map<String,String>> dataList;
	private Map<String,String> data;

	@BeforeSuite
	public void initSuite() {
		Lib.loadProperty();
		Reporter.initExtent();
	}

	@BeforeMethod
	public void initDriver() {
		String browser = Lib.getProperty("BROWSER").isEmpty() ? "chrome" : Lib.getProperty("BROWSER");
		switch (browser.toLowerCase()) {
		case "chrome":
			System.setProperty("webdriver.chrome.driver", Lib.getProperty("CHROME_DRIVER"));
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver(options);
		case "firefox":
			
			
		}
		driver.manage().timeouts().pageLoadTimeout(Long.parseLong(Lib.getProperty("PAGE_LOAD_TIMEOUT")), TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(Long.parseLong(Lib.getProperty("IMPLICIT_WAIT")), TimeUnit.SECONDS);
	}
	
	@BeforeMethod
	public void extractData(Method m) {
		this.dataList = new ArrayList<>();
		int header=0;
		try {
			XL_ReadWrite xl = new XL_ReadWrite(System.getProperty("user.dir") + "//TestData.xlsx");

			// Getting a pointer to the header
			for (int iRow = 0; iRow < xl.getRowCount(0); iRow++) {
				if (xl.getCellData(0, iRow, 0).equals(m.getName())) {
					header = iRow; break;
				}
			}
			
		    if(header == 0) throw new Exception("Test case "+m.getName()+" not found in test data sheet");
			for (int iRow = header; iRow < xl.getRowCount(0); iRow++) {
				if (xl.getCellData(0, iRow, 0).equals(m.getName())) {	
					this.data = new HashMap<>();
					for (int iCol = 1; iCol < xl.getColCount(header, 0); iCol++) {
						data.put(xl.getCellData(0, header, iCol), xl.getCellData(0, iRow+1, iCol));
					}
					dataList.add(data);
				}else if(!dataList.isEmpty() & !xl.getCellData(0, iRow, 0).equals(m.getName())) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@BeforeMethod
	public void startTestReport(Method m) {
		Reporter.startTest(m.getName());
	}
	
	@AfterMethod
	public void endTestReport(Method m) {
		Reporter.endTest();
	}
	
	@AfterClass
	public void flushReport() {
		Reporter.flushTest();
	}
	
	@AfterSuite
	public void closeReport() {
		Reporter.testClose();
	}
	
	@AfterMethod
	public void quitDriver() {
		if(driver != null) driver.quit();
	}
	
	public void reportException(Exception ex) {
		Reporter.log(RepStatus.FAIL, "Exception", ex.getMessage());
		Assert.assertFalse(true, ex.getMessage());
	}

}
