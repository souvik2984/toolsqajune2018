package util;

public enum RepStatus {
	PASS,
	FAIL,
	WARN,
	INFO
}
