package util;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Reporter {
	
	private static ExtentReports extent;
	private static ExtentTest test;
	
	public static void initExtent() { // Test base
		extent = new ExtentReports(System.getProperty("user.dir")+"//TestReport.html", true);
	}
	
	public static void startTest(String testName) { // Test base
	    test =  extent.startTest(testName);	
	}
	
	public static void log(RepStatus status, String step,String desc,boolean... screen) { // Test Developer
		String imgPath="";
		if(screen[0]) {
			// call method for capturing screen
		}
		
		switch(status) {
			//case PASS : test.log(LogStatus.PASS, step, desc+test.addScreenCapture(imgPath));break;
			case PASS : test.log(LogStatus.PASS, step, desc);break;
			case FAIL : test.log(LogStatus.FAIL, step, desc);break;
			case WARN : test.log(LogStatus.WARNING, step, desc);break;
			case INFO : test.log(LogStatus.INFO, step, desc);break;
		}
	}
	
	public static void endTest() { // Test Base
		extent.endTest(test);
	}
	
	public static void flushTest() { // Test Base
		extent.flush();
	}
	
	public static void testClose() { // Test base 
		extent.close();
	}
}
