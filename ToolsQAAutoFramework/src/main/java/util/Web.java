package util;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Web {
	
	private static String tempVal;
	
	public static String getTempVal() {
		return tempVal;
	}

	public static void setTempVal(String tempVal) {
		Web.tempVal = tempVal;
	}

	public static void selectElementByVisibleText(WebElement ele,String option) {
		Select sel = new Select(ele);
		sel.selectByVisibleText(option);
	}
	
	public static void selectElementByIndex(WebElement ele,int option) {
		Select sel = new Select(ele);
		sel.selectByIndex(option);
	}
	
	public static void sendText(WebElement ele,String text) {
		ele.sendKeys(text);
	}
	
	public static void clickElement(WebElement ele) {
		ele.click();
	}
	
	public static String getText(WebElement ele) {
		return ele.getText();
	}
	
	public static WebElement waitForElement(WebDriver driver,WebElement e,int timeout) {
		return new WebDriverWait(driver,timeout)
				.ignoring(NoSuchElementException.class)
				.until(ExpectedConditions.elementToBeClickable(e));
	}
}
